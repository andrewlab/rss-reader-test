<?php

namespace App\Http\Controllers;

use Feeds;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     */
    public function index()
    {
        $feed = Feeds::make(' https://www.theregister.co.uk/software/headlines.atom', 0, true);

        $items = [];
        $wordTop = [];
        foreach ($feed->get_items() as $item)
        {
            $itemWordStats = $this->getWordTop($item->get_description());
            foreach ($itemWordStats as $word => $amount)
            {
                if (!array_key_exists($word, $wordTop)) {
                    $wordTop[$word] = 0;
                }
                $wordTop[$word] += $amount;
            }
        }
        arsort($wordTop);

        return view('feed', [
            'title' => $feed->get_title(),
            'permalink' => $feed->get_permalink(),
            'items' => $feed->get_items(),
            'wordTop' => array_slice($wordTop, 0, 10, true)
        ]);
    }

    private function getTopMostCommonEnglistWords(int $limit = null)
    {
        $words = [
            "the",
            "be",
            "to",
            "of",
            "and",
            "a",
            "in",
            "that",
            "have",
            "I",
            "it",
            "for",
            "not",
            "on",
            "with",
            "he",
            "as",
            "you",
            "do",
            "at",
            "this",
            "but",
            "his",
            "by",
            "from",
            "they",
            "we",
            "say",
            "her",
            "she",
            "or",
            "an",
            "will",
            "my",
            "one",
            "all",
            "would",
            "there",
            "their",
            "what",
            "so",
            "up",
            "out",
            "if",
            "about",
            "who",
            "get",
            "which",
            "go",
            "me",
            "when",
            "make",
            "can",
            "like",
            "time",
            "no",
            "just",
            "him",
            "know",
            "take",
            "people",
            "into",
            "year",
            "your",
            "good",
            "some",
            "could",
            "them",
            "see",
            "other",
            "than",
            "then",
            "now",
            "look",
            "only",
            "come",
            "its",
            "over",
            "think",
            "also",
            "back",
            "after",
            "use",
            "two",
            "how",
            "our",
            "work",
            "first",
            "well",
            "way",
            "even",
            "new",
            "want",
            "because",
            "any",
            "these",
            "give",
            "day",
            "most",
            "us",
        ];

        return array_splice($words, 0, $limit);
    }

    private function getWordTop(string $source)
    {
        $source = strip_tags($source);
        $source = preg_replace("/[^A-Za-z0-9 ]/", '', $source);

        $res = str_word_count($source, 2);
        $res = array_diff($res, $this->getTopMostCommonEnglistWords(50));
        $res = array_combine(array_values($res), array_keys($res));

        return $res;
    }
}
