@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="header">
                    Source: <h1><a href="{{ $permalink }}">{{ $title }}</a></h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                    @foreach ($wordTop as $word => $amount)
                    {{$word}}({{$amount}}),
                    @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @foreach ($items as $item)
                    <div class="card">
                        <div class="card-header">
                            <h2><a href="{{ $item->get_permalink() }}">{{ $item->get_title() }}</a></h2>
                        </div>

                        <div class="card-body">
                            <p>{!! $item->get_description() !!}</p>
                            <p><small>Posted on {{ $item->get_date('j F Y | g:i a') }}</small></p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
