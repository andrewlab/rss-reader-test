require('./bootstrap');

window.Vue = require('vue');

Vue.component('registration-form', require('./components/auth/RegistrationFormComponent.vue').default);

const app = new Vue({
    el: '#app'
});
