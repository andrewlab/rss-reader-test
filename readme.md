# Rss reader test

## Task
To create simple RSS reader web application with following views
1) User registration - form with e-mail and password fields + e-mail verification using ajax.
Existence of already registered e-mail should be checked �on the fly� via ajax call when writing e-mail address and before submitting form.
2) Login form with e-mail address and password
3) RSS feed view (Feed source: https://www.theregister.co.uk/software/headlines.atom)
     *) After successful login in top section display 10 most frequent words with their respective counts in the whole feed excluding top 50 English common words (taken from here https://en.wikipedia.org/wiki/Most_common_words_in_English)
     *)Underneath create list of feed items.

There are no restrictions on frameworks (PHP and/or JS) used.
When doing this task please apply the best practices in software development.

## Improvements that could be done
- Optimize word counter algorythm (Caching/lower level rewrite/more performant language usage)
- Test coverage
- Automate the environment setup (docker, build scripts)

## Dev build requirements
- PHP 7.1 (https://laravel.com/docs/5.8/installation)
- Composer

## Setup
- .env file in root directory
```
APP_NAME=RssReaderTaskIn2019
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=sqlite

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120
```
- add empty file: /database/database.sqlite
- composer install
- php artisan key:generate
- php artisan migrate
- php artisan serve

The page should be available by default under http://127.0.0.1:8000

## Troubleshooting
- ensure writeable: ./storage, ./bootstrap/cache, .database/database.sqlite

## Feedback
- +) setup was straight forward
- +) it works
- +) code was easy to read and logical
- +) Nice that Vue.js was used
- *) Some logic (i.e. word extraction & counting) could be take out from controller in separate testable service
- *) There were some number constants that could be take out to parameters

Overall good job!
